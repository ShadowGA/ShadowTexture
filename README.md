# Shadow.GA
A modern creative server for Minecraft.
<br>
web: https://shadow.ga
<br>
social: https://twitter.com/ShadowGADev
<br>
<br>
ShadowTexture is the official resource pack designed to work with Shadow.GA. It uses newly available 32x32 pixel-by-pixel resource files to change the look and feel of Minecraft. While still remaining alike to the original textures to the point where they do not change builds without any resource pack, they give enough identity to build outside the limits of normal Minecraft.
